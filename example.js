import React, { Component, PropTypes} from 'react'
import { Link } from 'react-router-dom'
import Post from './Post'
import { API } from '../config'
import SearchInput, {createFilter} from 'react-search-input'
// import { Map } from 'immutable'

const GET_ALL_POST = API.baseUri+API.initPosts;

const KEYS_TO_FILTERS = ['idUSER','userName','content','category','urlImage','idSHOP','name','dateTime','idQUICKORDER']
const KEY_TO_FILTERS = ['category']
export default class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            posts: [],
            search:'',
            searchTerm: '',
            favorId: [],
            userID:3,
            KEY_FILTER:[],
        }
    }

    updateSearch(event){
        this.setState({
            search:event.target.value,
        })

    }

    componentDidMount(){
        this.getData()
    }
    //get post data
    getData() {
        //get all initial posts
        fetch(API.baseUri + API.initPosts)
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                } else console.log("Get data error ");
            }).then((json) => {
            // console.log(json)
            this.setState({posts: json})
            console.log(this.state.posts)
        }).catch((error) => {
            console.log('error on .catch', error);
        })
            //put the postId in to an array
            .then(() => {
            var tempFavor = this.state.posts;
            var tempFavorite = this.state.favorId;
            for(var i in tempFavor){
                if (tempFavor[i].idPOST !== null){
                    // tem = tempFavor[i].idPOST;
                    tempFavorite.push(tempFavor[i].idPOST);
                    this.setState({
                        favorId:tempFavorite
                    });
                }
            }
            console.log(this.state.favorId)
        })

    }

    callBackDP = (index) =>{
        this.state.posts.splice(index,1);
        this.forceUpdate();
        console.log("打印当前所有post", this.state.posts);
      // console.log("callback", this.state.posts);

    }

    //分类
    categorylist(cate){
        this.setState({
            searchTerm: cate,
            KEY_FILTER:KEY_TO_FILTERS
        })

      }

    searchUpdate(term){

            this.setState({
                searchTerm:term,
                // KEY_FILTER:KEYS_TO_FILTERS
            })
    }
    //get the latest search words
    searchUpdated (term) {
        this.setState({searchTerm: term})
    }

  render() {
        return (
        <div>
            {/* <!-- Top service filter nav bar --> */}
              <div className="nova-margin">
                  <div className="nova-padding service-header">
                        {/* Filter posts using these buttons */}
                      <div className="service"
                            onClick={()=>{this.categorylist("租房信息")}}
                      ><i className="fa fa-home"></i>租房信息</div>
                      <div className="service"
                           onClick={()=>{this.categorylist("车辆交易")}}
                      ><i className="fa fa-car"></i>车辆交易</div>
                      <div className="service"
                           onClick={()=>{this.categorylist("二手信息")}}
                      ><i className="fab fa-sellcast"></i>二手信息</div>
                      <div className="service"
                           onClick={()=>{this.categorylist("生活服务")}}
                      ><i className="fa fa-newspaper"></i>生活服务</div>
                      <div className="service"><Link to='/post/newpost'><i className="far fa-edit"></i>发布</Link></div>
                  </div>
              </div>

              <div className="nova-margin">
                  <div className="nova-padding filter">
                      <ul>
                          <li className="choose">出租</li>
                          <li>求租</li>
                          <li className="choose">长租</li>
                          <li>临时</li>
                          <div className="input-group">
                              {/* <input type="text" className="form-control" placeholder="关键词筛选..."
                                     value={this.state.search} onChange={(event)=>{this.updateSearch(event)}}/> */}
                              <SearchInput type="text"
                                           className="form-control"
                                           placeholder="关键词筛选..."
                                           className="search-input"
                                           onChange={this.searchUpdated} />
                              <span className="input-group-btn">
                                  <button className="btn btn-default" type="button"
                                    onClick={()=>this.searchUpdate(this.state.search)}
                                  >筛选</button>
                              </span>
                          </div>
                      </ul>
                  </div>
              </div>
              <div className="posts nova-margin">
                <Post
                    posts={this.state.posts}
                    allFav={this.state.favorId}
                    userId={this.state.userID}
                    callback_DeleteidPost={this.callBackDP}
                    searchTerm={this.state.searchTerm}
                    serchUpdate={this.state.searchTerm}
                    callback_DeleteidPost={this.callBackDP}
                />
              </div>
            </div>
    )
  }
}
