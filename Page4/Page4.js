import React, {Component} from 'react';
import { Text,View, Button } from 'react-native';
import Post from '../Post/Post';
import {StackNavigator} from 'react-navigation';
class Page4 extends Component {
  
  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;
    return{
      headerRight: (
        <Button title="Post" color="#fff" onPress= {()=> {navigation.navigate('Post')}} />
 
      ),
    };
  }

  render () {
    return (
     <View>
       <Text>不知道什么页面</Text>
       </View>
    );
  }
}
const Stack=StackNavigator(
  {
    Home:{
      screen:Page4,
    },
    Post: {
      screen: Post,
    }
  },

  {
    initialRouteName: 'Home',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  }

);
export default Stack;

