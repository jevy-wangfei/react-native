import React, { Component } from 'react';
import {  View, Text, StyleSheet, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, navigation } from 'react-native';
import { Button } from 'react-native-elements';
import Page1 from '../Page1/Page1';
//import Loginform from './Loginform';
//import Logo from '../Image/Logo';

export default class Loginform extends Component {
    constructor(props){
        super(props)
        this.state={
            username: '',
            password: '',
            status: 'normal'
        }
    }

    handleUserNameChange = (event) => {
        this.setState({
            username: event.target.value
        })
    }
    handleUserPWChange = (event) => {
            this.setState({
                password: event.target.value
            })
        }

    handleErr = (stat) =>{
            if (stat === 'normal'){
                return (<label></label>);
            }
            else if (stat ==='failed'){
                return (<label>The email or password is incorrect</label>);
            }
            else{
                return (<label>Log in successful</label>);
            }
    }

     //Submit the new Comment
     handleSubmit=()=> {
        console.log(this.state.username);
        console.log(this.state.password);
        this.setState({status: 'normal'})
        var API = "http://localhost:8080/users/login";
        fetch(API, {
        //credentials: 'include',
            method: "POST",
            headers: {
                //     'Content-Type':'multipart/form-data',
                //     'Content-Type':'application/x-www-form-urlencoded',
                //     'Content-Disposition': 'form-data',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({

                // this.state.UserID,
                "email": this.state.username,
                "password": this.state.password,

            })
        }).then(res => {
            if(res.status === 200){
                this.setState({status: 'success'})
                return res.json()
            }else{
                this.setState({status: 'failed'})
                return res.json()
            }

            }
        ).then(json =>{
            //console.log(json.email)
            localStorage.setItem(json.email, JSON.stringify(json))
            //console.log(localStorage.getItem(json.email))

    }).catch(error => {
                console.error(error)
            })

    }

    handleLogout=()=> {
        //console.log(this.state.username);
        //console.log(this.state.password);
        //this.setState({status: 'normal'})
        var API = "http://localhost:3002/users/logout";
        fetch(API, {
            // //credentials: 'include',
            // method: "GET"
            // // headers: {
            // //      //    'Content-Type':'multipart/form-data',
            // //          'Content-Type':'application/x-www-form-urlencoded',
            // //     //     'Content-Disposition': 'form-data',
            // //     //'Accept': 'application/json',
            // //     //'Content-Type': 'application/json'
            // // }

        }).then(res => {

                    return res.json()
            }
        ).then(json =>{
            console.log(json)
            //localStorage.setItem(json.email, JSON.stringify(json))
            //console.log(localStorage.getItem(json.email))

        }).catch(error => {
            console.error(error)
        })

    }

  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
       <View style={styles.logoContainer}>
            <Text style={styles.title}>Forum Login Page</Text>
        </View>
      <View style={styles.formContainer}>
        <TextInput
        placeholder="username or email"
        returnKeyType="next"
        onSubmitEditing={() => this.passwordInput.focus()}
        opacity='0.9'
        autoCapitalize="none"
        autoCorrect={false}
            style={styles.input}
        />
        <TextInput
        placeholder="password"
        returnKeyType="go"
        opacity='0.9'
        secureTextEntry
            style={styles.input}
        ref={(input)=> this.passwordInput = input}
        />
        <Button 
            backgroundColor={'#666a6c'}
            title='Login' style={styles.buttonsetting} 
            //onPress={() => {this.handleSubmit()}}
            />
        <Button 
            backgroundColor={'#666a6c'}
            title='Cancel' style={styles.buttonsetting}
            onPress= {()=> {
                console.log(this.props)
                this.props.navigation.navigate('Page1')
            }} 
            />

       </View>
       <View style={styles.buttonContainer}>
       <TouchableOpacity style={styles.buttonContainer}>
            <Text style={styles.textContainer}>Login with Google Account</Text>
        </TouchableOpacity>
        </View>   
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#e0e5e8'
    },
    logoContainer:{
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    title:{
        color:'#6f6f6f',
        marginTop: 10,
        width: 150,
        textAlign: 'center',
        opacity: 0.9,
        
    },
    formContainer:{
        padding: 20,
    },
    input:{
        height: 40,
        backgroundColor: '#b7bec3',
        marginBottom: 20,
    },
    buttonsetting:{
        marginBottom: 10,
        //backgroundColor: '#9E9E9E',
        height: 50,
        
    },
    buttonContainer:{
        height: 40,
        marginBottom: 10,
        alignItems: 'center',
       
    },
    textContainer:{
        fontWeight: '700',
    }
})

